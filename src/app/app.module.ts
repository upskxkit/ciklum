import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent } from './components/form/form.component';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { InputComponent } from './components/input/input.component';
import { SplitWordsPipe } from './pipes/split-words.pipe';
import { ReadonlyComponent } from './components/readonly/readonly.component';
import { View404Component } from './components/view404/view404.component';

const routes: Routes = [
  {path: '', component: FormComponent},
  {path: 'readonly', component: ReadonlyComponent},
  {path: '**', component: View404Component}
];

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    InputComponent,
    SplitWordsPipe,
    ReadonlyComponent,
    View404Component
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    RouterModule.forRoot(routes),
    SweetAlert2Module.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
