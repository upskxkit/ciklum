import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {RouterTestingModule} from '@angular/router/testing';
import {By} from '@angular/platform-browser';
import {Component, DebugElement, Directive, HostListener, Input, NO_ERRORS_SCHEMA} from '@angular/core';
import {Routes} from '@angular/router';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [RouterTestingModule]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have nav`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    expect(fixture.debugElement.query(By.css('nav'))).not.toBeNull();
  });

  it(`should have router-outlet`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    expect(fixture.debugElement.query(By.css('router-outlet'))).not.toBeNull();
  });


});

@Directive({
  selector: '[routerLink]'
})
export class RouterLinkDirectiveStub {
  @Input('routerLink') linkParams: any;
  navigatedTo: any = null;

  @HostListener('click')
  onClick() {
    this.navigatedTo = this.linkParams;
  }
}

@Component({
  template: `Form`
})
export class FormStubComponent {
}

@Component({
  template: `other`
})
export class View404StubComponent {
}

@Component({
  template: `Readonly`
})
export class ReadonlyStubComponent {
}

describe('Router: AppComponent', () => {

  let fixture, linkDes, routerLinks;
  const routes: Routes = [
    {path: '', component: FormStubComponent},
    {path: 'readonly', component: ReadonlyStubComponent},
    {path: '**', component: View404StubComponent}
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        FormStubComponent,
        ReadonlyStubComponent,
        View404StubComponent,
        RouterLinkDirectiveStub
      ],
      imports: [RouterTestingModule.withRoutes(routes)],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();

    linkDes = fixture.debugElement.queryAll(By.directive(RouterLinkDirectiveStub));
    routerLinks = linkDes.map(de => de.injector.get(RouterLinkDirectiveStub));
  }));

  it('can get RouterLinks from template', () => {

    expect(routerLinks.length).toBe(2, 'should have 2 routerLinks');
    expect(routerLinks[0].linkParams).toBe('/');
    expect(routerLinks[1].linkParams).toBe('/readonly');
  });

  it('can click navbar link in template', () => {

    const readonlyLinkDe: DebugElement = linkDes[1];
    const readonlyLink = routerLinks[1];
    expect(readonlyLink.navigatedTo).toBeNull('should not have navigated yet');

    readonlyLinkDe.triggerEventHandler('click', {button: 0});
    fixture.detectChanges();

    expect(readonlyLink.navigatedTo).toBe('/readonly');

  });

});
