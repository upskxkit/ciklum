import {Component, Input} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Item} from '../form/form.component';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent {

  @Input() item: Item = null;
  @Input() form: FormGroup = null;

  constructor() {
  }

}
