import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {InputComponent} from './input.component';
import {FormControl, FormGroup, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SplitWordsPipe} from '../../pipes/split-words.pipe';
import {By} from '@angular/platform-browser';

describe('InputComponent', () => {
  let component: InputComponent;
  let fixture: ComponentFixture<InputComponent>;
  const checkbox = {type: 'checkbox', controlName: 'verifyEmail'};
  const string = {type: 'input', controlName: 'defaultLanguage'};
  const select = {
    type: 'select',
    options: ['ignore',
      'failOnSiteConflictingIdentity',
      'failOnAnyConflictingIdentity'],
    controlName: 'loginIdentifierConflict'
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InputComponent, SplitWordsPipe],
      imports: [FormsModule, ReactiveFormsModule]
    })
      .compileComponents();

    fixture = TestBed.createComponent(InputComponent);
    component = fixture.componentInstance;

  }));


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('DOM: should have item as type input ', () => {

    expect(component.item).toBeNull();
    expect(component.form).toBeNull();

    component.form = new FormGroup({
      [string.controlName]: new FormControl('EN')
    });

    component.item = string;

    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css(`#${string.controlName}`)).nativeElement.value).toBe('EN');
    expect(component.item).toBe(string);
    expect(component.form).not.toBeNull();
  });

  it('should have item as type input ', () => {
    expect(component.item).toBeNull();

    component.item = string;

    expect(component.item).toBe(string);

  });

  it('DOM: should have item as type select ', () => {

    expect(component.item).toBeNull();
    expect(component.form).toBeNull();

    component.form = new FormGroup({
      [select.controlName]: new FormControl(select.options[0])
    });

    component.item = select;

    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css(`#${select.controlName}`)).nativeElement.value).toBe(select.options[0]);
    expect(component.item).toBe(select);
    expect(component.form).not.toBeNull();

  });

  it('should have item as type select ', () => {
    expect(component.item).toBeNull();

    component.item = select;

    expect(component.item).toBe(select);

  });

  it('DOM: should have item as type checkbox ', () => {

    expect(component.item).toBeNull();
    expect(component.form).toBeNull();

    component.form = new FormGroup({
      [checkbox.controlName]: new FormControl(true)
    });

    component.item = checkbox;

    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css(`#${checkbox.controlName}`)).nativeElement.checked).toBeTruthy();
    expect(component.item).toBe(checkbox);
    expect(component.form).not.toBeNull();

    component.form.controls[checkbox.controlName].setValue(false);

    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css(`#${checkbox.controlName}`)).nativeElement.checked).toBeFalsy();

  });

  it('should have item as type checkbox ', () => {
    expect(component.item).toBeNull();

    component.item = checkbox;

    expect(component.item).toBe(checkbox);
  });

});

