import {async, ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';

import {FormComponent} from './form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Component, DebugElement, Input} from '@angular/core';
import {AccountOptionsService, JsonpResponse, Policy} from '../../services/account-options.service';
import {Observable, of} from 'rxjs';
import {By} from '@angular/platform-browser';

@Component({
  selector: 'app-input',
  template: '<div>Input component</div>'
})
class InputStubComponent {
  @Input() item;
  @Input() form;
}

const JsonpResponse: JsonpResponse = {
  statusCode: 200,
  errorCode: 0,
  statusReason: 'OK',
  callId: '2b4faf66edbd459a95105fdb4b99e08b',
  time: '2019-04-19T20:03:38.876Z'
};

let accountOptionStubService: Partial<AccountOptionsService>;
accountOptionStubService = {
  getPolicies(): Observable<Policy> {
    return of<Policy>({
      accountOptions: {
        verifyEmail: true,
        verifyProviderEmail: true,
        allowUnverifiedLogin: true,
        preventLoginIDHarvesting: true,
        sendWelcomeEmail: true,
        sendAccountDeletedEmail: true,
        defaultLanguage: 'EN',
        loginIdentifierConflict: 'ignore',
        loginIdentifiers: 'email'
      }
    });
  },
  setPolicies(): Observable<JsonpResponse> {
    return of<JsonpResponse>(JsonpResponse);
  }
};

describe('FormComponent', () => {
  let component: FormComponent;
  let fixture: ComponentFixture<FormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormComponent, InputStubComponent],
      imports: [FormsModule, ReactiveFormsModule],
      providers: [{provide: AccountOptionsService, useValue: accountOptionStubService}]
    })
      .compileComponents();

    fixture = TestBed.createComponent(FormComponent);
    component = fixture.componentInstance;
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('is form has title', () => {
    const componentDE: DebugElement = fixture.debugElement;

    expect(componentDE.query(By.css('h1')).nativeElement.textContent).toBe('Account Options');

  });

  it('is form valid when empty', () => {
    expect(component.form).toBeTruthy();
  });

  it('should get Policy', fakeAsync(() => {

    expect(component.pending).toBeTruthy();

    component.ngOnInit();

    expect(component.pending).toBeFalsy();
    expect(component.buildType.length).toBeGreaterThan(0);

  }));

  it('should submit form', () => {
    fixture.detectChanges();

    expect(component.pending).toBeFalsy();

    component.submitted.subscribe(data => {
      expect(data).toEqual(JsonpResponse);

      expect(component.pending).toBeFalsy();
    });

    component.submit();

  });

});
