import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AccountOptionsService } from '../../services/account-options.service';
import Swal from 'sweetalert2';

enum CONTROL_TYPE {
  INPUT    = 'input',
  SELECT   = 'select',
  CHECKBOX = 'checkbox'
}

export interface Item {
  type?: string;
  options?: string[];
  controlName?: string;
}

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  @Input() disabled = false;

  public pending = true;
  public form: FormGroup = this.fb.group({});
  public buildType: Item[] = [];
  @Output() public submitted = new EventEmitter();
  private loginIdentifierConflictOtions = ['ignore', 'failOnSiteConflictingIdentity', 'failOnAnyConflictingIdentity'];

  constructor(private accOptService: AccountOptionsService,
              private fb: FormBuilder) {
  }

  public ngOnInit() {
    this.accOptService.getPolicies().subscribe(({accountOptions}) => {
      const keys = Object.keys(accountOptions);

      for (let i = 0; i < keys.length; i++) {
        const key = keys[i];
        let item: Item = {};

        if (typeof accountOptions[key] === 'string') {

          this.form.addControl(key, this.fb.control({value: accountOptions[key], disabled: this.disabled}));

          if (key === 'loginIdentifierConflict') {
            item = {type: CONTROL_TYPE.SELECT, options: this.loginIdentifierConflictOtions};
          } else {
            item = {type: CONTROL_TYPE.INPUT};
          }

        } else if (typeof accountOptions[key] === 'boolean') {

          this.form.addControl(key, this.fb.control({value: accountOptions[key], disabled: this.disabled}));

          item = {type: CONTROL_TYPE.CHECKBOX};
        }

        item = {...item, controlName: key};

        this.buildType.push(item);
      }

      this.pending = false;

    });

    this.submitted.subscribe(data => {
      this.pending = false;

      if (!!data.errorCode) {

        console.error(`Error code: ${ data.errorCode },
        Error detail: ${ data.errorDetails }`);

        Swal.fire({
          type: 'error',
          title: data.statusReason,
          text: data.errorMessage, timer: 2000
        });
        return;
      }

      Swal.fire({type: 'success', text: 'You have updated Account options.', timer: 2000});
    });
  }

  public submit() {
    this.pending = true;
    const accountOptions = this.form.value;

    this.accOptService.setPolicies(accountOptions)
        .subscribe(data => this.submitted.emit(data));

  }

}
