import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReadonlyComponent } from './readonly.component';
import { Component, Input } from '@angular/core';
import { By } from '@angular/platform-browser';

@Component({
  selector: 'app-form',
  template: '<div>Account Options Test</div>'
})
class FormStubComponent {
  @Input() disabled;
}

describe('ReadonlyComponent', () => {
  let component: ReadonlyComponent;
  let fixture: ComponentFixture<ReadonlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReadonlyComponent, FormStubComponent],
    })
           .compileComponents();

    fixture = TestBed.createComponent(ReadonlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain app-form', () => {

    expect(fixture.debugElement.query(By.css('app-form'))).toBeTruthy();

    const appForm: HTMLElement = fixture.debugElement.query(By.css('app-form')).nativeElement;

    expect(appForm.textContent).toContain('Account Options Test');

  });

});
