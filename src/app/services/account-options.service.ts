import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

enum METHODS {
  GETPOLICIES = 'getPolicies',
  SETPOLICIES = 'setPolicies',
}

export interface Policy {
  accountOptions: AccountOptions;
}

export interface JsonpResponse {
  statusCode: number;
  errorCode: number;
  statusReason: string;
  callId: string;
  time: string;
  errorMessage?: string;
  errorDetails?: string;
}

export interface AccountOptions {
  verifyEmail: boolean;
  verifyProviderEmail: boolean;
  allowUnverifiedLogin: boolean;
  preventLoginIDHarvesting: boolean;
  sendWelcomeEmail: boolean;
  sendAccountDeletedEmail: boolean;
  defaultLanguage: string;
  loginIdentifierConflict: string;
  loginIdentifiers: string;
}

@Injectable({
  providedIn: 'root'
})
export class AccountOptionsService {
  constructor(private http: HttpClient) {
  }

  public getPolicies(): Observable<Policy> {
    const url = this.url_builder(METHODS.GETPOLICIES);

    return this.http.jsonp<Policy>(url, 'callback');
  }

  public setPolicies(options: any) {
    const url = this.url_builder(METHODS.SETPOLICIES, options);

    return this.http.jsonp<JsonpResponse>(url, 'callback');
  }

  private url_builder(method: METHODS, accountOptions?: AccountOptions): string {

    const domain = `${environment.apiUrl}/${environment.subUrl}`;

    let options_url = '';

    const options = {
      userkey: encodeURIComponent(environment.userKey),
      secret: encodeURIComponent(environment.userSecret),
      apikey: encodeURIComponent(environment.apiKey),
      format: 'jsonp',
      callback: environment.callback
    };

    if (!!accountOptions) {
      options['accountOptions'] = encodeURIComponent(this.optionsToString(accountOptions));
    }

    for (const key in options) {
      options_url += `${key}=${options[key]}&`;
    }
    return `${domain}.${method}?${options_url}`.replace(/&$/g, '');
  }

  private optionsToString(options) {

    let resp = '{';

    for (const key in options) {

      if (typeof options[key] === 'string') {
        resp += `${key}:"${options[key]}",`;
      } else {
        resp += `${key}:${options[key]},`;
      }

    }

    resp += '}';

    return resp;
  }

}
