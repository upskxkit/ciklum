import {AccountOptionsService, JsonpResponse, Policy} from './account-options.service';
import {of} from 'rxjs';

describe('AccountOptionsService', () => {

  let httpClientSpy: { jsonp: jasmine.Spy };
  let accountOptionsService: AccountOptionsService;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['jsonp']);
    accountOptionsService = new AccountOptionsService(<any>httpClientSpy);
  });

  it('should be created', () => {
    expect(accountOptionsService).toBeTruthy();
  });

  it('should return Policy (HttpClient called once)', () => {
    const policy: Policy = {
      accountOptions: {
        verifyEmail: true,
        verifyProviderEmail: true,
        allowUnverifiedLogin: true,
        preventLoginIDHarvesting: true,
        sendWelcomeEmail: true,
        sendAccountDeletedEmail: true,
        defaultLanguage: 'EN',
        loginIdentifierConflict: 'ignore',
        loginIdentifiers: 'email'
      }
    };

    httpClientSpy.jsonp.and.returnValue(of(policy));

    accountOptionsService.getPolicies().subscribe(data => {
      expect(data).toBe(policy);
    });

    expect(httpClientSpy.jsonp.calls.count()).toBe(1, 'one call');
  });

  it('should return jsonpResponse (HttpClient called once)', () => {
    const jsonpResponse: JsonpResponse = {
      statusCode: 200,
      errorCode: 0,
      statusReason: 'OK',
      callId: '2b4faf66edbd459a95105fdb4b99e08b',
      time: '2019-04-19T20:03:38.876Z'
    };

    httpClientSpy.jsonp.and.returnValue(of(jsonpResponse));

    accountOptionsService.setPolicies(null).subscribe(data => {
      expect(data).toBe(jsonpResponse);
    });

    expect(httpClientSpy.jsonp.calls.count()).toBe(1, 'one call');

  });

});
