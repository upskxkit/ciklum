import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'splitWords'
})
export class SplitWordsPipe implements PipeTransform {

  transform(value: string, splitBy?: any): any {

    if (!value.length) {
      return '';
    }

    switch (splitBy) {

      case 'upperCase': {
        return value.replace(/([a-z])([A-Z])/gm, this.replacer);
      }

      case 'underscore': {
        return value.replace(/_/gm, ' ');
      }

      case 'dash': {
        return value.replace(/-/gm, ' ');
      }

      default: {
        return value.replace(/([a-z])([A-Z])/gm, this.replacer);

      }

    }

  }

  private replacer(str, small, large) {
    return `${ small } ${ large.toLowerCase() }`;
  }

}
