import { SplitWordsPipe } from './split-words.pipe';

describe('SplitWordsPipe', () => {
  it('create an instance', () => {
    const pipe = new SplitWordsPipe();
    expect(pipe).toBeTruthy();
  });
});

describe('SplitWordsPipe values', () => {

  let pipe;

  beforeEach(() => {
    pipe = new SplitWordsPipe();
  });

  it('split value by upperCase', () => {

    const testString = 'testUpperCase';

    expect(pipe.transform(testString, 'upperCase')).toBe('test upper case');

  });

  it('split value by underscore', () => {

    const testString = 'test_upper_case';

    expect(pipe.transform(testString, 'underscore')).toBe('test upper case');

  });

  it('split value by dash', () => {

    const testString = 'test-upper-case';

    expect(pipe.transform(testString, 'dash')).toBe('test upper case');

  });

  it('split value by default', () => {

    const testString = 'testUpperCase';

    expect(pipe.transform(testString)).toBe('test upper case');

  });

  it('empty value', () => {

    const testString = '';

    expect(pipe.transform(testString)).toBe('');

  });


});
